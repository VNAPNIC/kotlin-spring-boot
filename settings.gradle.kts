rootProject.name = "kotlin-spring-boot"
include("common")
include("gateway-service")
include("discovery-service")
include("auth-service")
include("storage-service")
include("user-service")