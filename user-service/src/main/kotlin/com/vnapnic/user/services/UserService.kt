package com.vnapnic.user.services

interface UserService {
    fun findById(userId: String)
}